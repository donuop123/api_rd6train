<?php

namespace App\Services;

use Curl\Curl;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Log;

class ApiDataService
{
    protected $model;

    public function getApiData($start_datetime, $from)
    {
        $end_datetime = clone $start_datetime;
        $end_datetime->modify("+1 min");

        $starttime = $start_datetime->format("Y-m-d\TH:i:s");
        $endtime = $end_datetime->format('Y-m-d\TH:i:s');

        $params = [
            "start" => $starttime,
            "end" => $endtime,
            "from" => $from,
        ];
        $params = http_build_query($params);
        $params = str_replace("%3A", ":", $params);

        $curl = new Curl();
        $url = "http://train.rd6/?" . $params;
        $curl->get($url);
        if ($curl->error) {
            Log::channel("queue")->error("Error: " . $curl->errorCode . ": " . $curl->errorMessage);
        }
        echo 'Response:' . "\n";
        echo "{$url}\n";

        $response = json_decode($curl->response, true);

        $data_exist = $this->checkDateExist($response);

        $page = ($from / 10000) + 1;
        $data_length = count($response["hits"]["hits"]);
        echo "第{$page}頁資料總數: {$data_length}\n";

        $result = [];
        if ($data_exist) {
            $result = $this->processeData($response["hits"]["hits"]);
        }

        return $result;
    }

    public function checkDateExist($response)
    {
        $data_key = array_keys($response);
        if ($data_key[0] == "error" || $response["hits"]["hits"] == null) {
            echo "已查無資料或發生錯誤\n";
            Log::channel("queue")->info("已查無資料");
            return false;
        }
        return true;
    }

    public function processeData($data)
    {
        foreach ($data as $key => $value) {
            $source_timestamp = $value["_source"]["@timestamp"];
            $sub_datetime = substr($source_timestamp, 0, 19);

            $timestamp_obj = DateTime::createFromFormat("Y-m-d\TH:i:s", $sub_datetime);
            $timestamp_obj->setTimezone(new DateTimeZone("GMT-4"));

            $data[$key]["timestamp_US_Eastern"] = $timestamp_obj->format("Y-m-d H:i:s");
            $data[$key]["_source"] = json_encode($value["_source"]);
            $data[$key]["sort"] = json_encode($value["sort"]);
        }

        return $data;
    }

    public function isNextPage($response)
    {
        if (count($response) === 10000) {
            return true;
        }
        return false;
    }

    public function processedWagers($processed_data)
    {
        $result = [];

        foreach ($processed_data as $key => $value) {
            $processed_data[$key]["_source"] = json_decode($value["_source"], true);
            $processed_data[$key]["sort"] = json_decode($value["sort"], true);

            $timestamp_obj = new \DateTime();
            $timestamp_obj->setTimestamp(substr($processed_data[$key]["sort"][0], 0, 10));

            $result[$key]["unique_id"] = $value["_id"];
            $result[$key]["server_name"] = $processed_data[$key]["_source"]["server_name"];
            $result[$key]["route"] = $processed_data[$key]["_source"]["route"];
            $result[$key]["http_args"] = $processed_data[$key]["_source"]["http_args"];
            $result[$key]["timestamp"] = $timestamp_obj->format("Y-m-d H:i:s");
        }

        return $result;
    }

    public function getBetlogModel()
    {
        $model_name = "\\App\\Rd6ApiTrain";

        if (class_exists($model_name)) {
            $this->model = new $model_name;
        }

        return $this->model;
    }

    public function getWagersModel()
    {
        $model_name = "\\App\\ProcessedData";

        if (class_exists($model_name)) {
            $this->model = new $model_name;
        }

        return $this->model;
    }
}
