<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rd6ApiTrain extends Model
{
    protected $table = "rd6_api_trains";

    protected $fillable = ["_index", "_type", "_id", "_score", "_source", "sort", "timestamp_US_Eastern)"];

    protected $hidden = ["id", "created_at", "updated_at"];
}
