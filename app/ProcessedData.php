<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessedData extends Model
{
    protected $table = 'processed_data';

    protected $fillable = ["unique_id", "server_name", "route"];

    protected $hidden = ["id", "created_at", "updated_at"];
}
