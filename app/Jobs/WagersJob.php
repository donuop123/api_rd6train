<?php

namespace App\Jobs;

use App\Services\ApiDataService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WagersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $processed_data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($processed_data)
    {
        $this->processed_data = $processed_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ApiDataService $data_service)
    {
        $processed_data = $data_service->processedWagers($this->processed_data);

        $model = $data_service->getWagersModel();

        $repository = new \App\Repositories\ApiDataRepository($model);

        $repository->InsertOrUpdate($processed_data, "unique_id");
    }
}
