<?php

namespace App\Jobs;

use App\Services\ApiDataService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BetlogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    protected $model;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ApiDataService $data_service)
    {
        $model = $data_service->getBetlogModel();

        $repository = new \App\Repositories\ApiDataRepository($model);

        $processed_data = $repository->InsertOrUpdate($this->data, "_id");

        $job = (new WagersJob($processed_data))->onQueue("wagers");
        dispatch($job);
    }
}
