<?php

namespace App\Repositories;

use App\Rd6ApiTrain;
use Illuminate\Support\Facades\Log;

class ApiDataRepository
{
    protected $model;

    public function __construct(\Illuminate\Database\Eloquent\Model $model)
    {
        $this->model = $model;
    }

    public function InsertOrUpdate($data, $unique_key)
    {
        $processed_data = [];
        $sorted_data = $this->getInsertOrUpdateData($data, $unique_key);
        var_dump($sorted_data);

        if (!empty($sorted_data["insert"])) {
            echo "資料新增中...\n";
            foreach ($sorted_data["insert"] as $insert_data) {
                $this->model->insert($insert_data);
                $processed_data[] = $insert_data;
            }
            Log::channel("queue")->info("資料新增成功");
        }

        if (!empty($sorted_data["update"])) {
            foreach ($sorted_data["update"] as $update_data) {
                echo "資料更新中...\n";
                $this->model->where($unique_key, $update_data[$unique_key])->update($update_data);
                Log::channel("queue")->info("id: {$update_data[$unique_key]}更新成功");
                $processed_data[] = $update_data;
            }
        }

        return $processed_data;
    }

    public function getInsertOrUpdateData($data, $unique_key)
    {
        $data = collect($data);
        $data_ids = $data->pluck($unique_key);
        var_dump($data_ids);
        //  找出資料庫重複
        $exist_data = $this->model->whereIn($unique_key, $data_ids)->get();
        $exist_data_ids = $exist_data->pluck($unique_key)->all();

        $sorted_data["insert"] = $data->whereNotIn($unique_key, $exist_data_ids)->all();
        $duplicate_data = $data->whereIn($unique_key, $exist_data_ids);

        foreach ($exist_data->toArray() as $value) {
            $exist = $duplicate_data->search($value); //  重複資料中是否找得到與已存在資料庫一致的資料
            //  若資料一致則跳過不處理
            if (!($exist === false)) {
                continue;
            }

            $sorted_data["update"][] = $duplicate_data->where($unique_key, $value[$unique_key])->first();
            echo "此筆資料id: {$value[$unique_key]} 有被異動，需要更新\n";
        }

        return $sorted_data;
    }
}
