<?php

namespace App\Console\Commands;

use App\Jobs\QueueJob;
use DateInterval;
use DateTime;
use Illuminate\Console\Command;

class Rd6ApiTrain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:rd6apitrain
                            {--start= : format: YYYY-MM-DD\T00:00:00}
                            {--end= : format: YYYY-MM-DD\T00:00:59}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '輸入--start= --end= (ex: YYYY-MM-DD\T00:00:00), 預設為撈取本機時間前五分鐘資料';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start_datetime = $this->option("start");
        $end_datetime = $this->option("end");

        if (empty($start_datetime) && empty($end_datetime)) {
            $start_datetime = $this->defaultStartTime();
            $end_datetime = $this->defaultEndTime($start_datetime);
        } else {
            $start_datetime = $this->validateDatetime($start_datetime);
            $end_datetime = $this->validateDatetime($end_datetime);
            $this->validateInterval($start_datetime, $end_datetime);
        }

        while ($start_datetime < $end_datetime) {
            $job = (new QueueJob($start_datetime))->onQueue("dataload");
            dispatch($job);

            $start_datetime->modify("+1 min");
        };
    }

    public function validateDatetime($datetime)
    {
        $datetime_obj = DateTime::createFromFormat("Y-m-d\TH:i:s", $datetime);
        $datetime_now = new DateTime();
        
        if (empty($datetime)) {
            throw new \Exception("開始與結束時間都要輸入!");
        }

        if (!($datetime_obj && $datetime_obj->format("Y-m-d\TH:i:s") == $datetime)) {
            throw new \Exception("時間輸入錯誤! (格式為: YYYY-MM-DD\T00:00:00)");
        } elseif ($datetime_obj > $datetime_now) {
            throw new \Exception("超過現在時間!");
        }

        return $datetime_obj;
    }

    public function validateInterval($start_datetime, $end_datetime)
    {
        if ($start_datetime >= $end_datetime) {
            throw new \Exception("endtime 不能大於 starttime!");
        }
    }

    public function defaultStartTime()
    {
        $start_datetime = new DateTime("5 min ago");
        return $start_datetime->setTime($start_datetime->format("H"), $start_datetime->format("i"), 0);
    }

    public function defaultEndTime($start_datetime)
    {
        $end_datetime = clone $start_datetime;
        $end_datetime->add(new DateInterval("PT5M"));
        return $end_datetime->setTime($end_datetime->format("H"), $end_datetime->format("i"), 0);
    }

}
